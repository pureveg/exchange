package routers

import (
	"bitbucket.org/pureveg/exchange/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.RESTRouter("/data/user", &controllers.UserRouter{})

	// login, POST
	beego.RESTRouter("/data/user/login", &controllers.LoginController{})
	// news, GET POST DELETE
	beego.RESTRouter("/data/news", &controllers.NewsControllers{})
	// ticker
	beego.RESTRouter("/data/ticker", &controllers.TickerController{})
	// history
	beego.RESTRouter("/data/history", &controllers.HistoryController{})
	// trade, GET POST DELETE
	beego.RESTRouter("/data/trade", &controllers.TradeController{})
}
