package models

import (
	"bitbucket.org/pureveg/db"
	"os"
)

var d *db.DB

func initDB() {
	d = db.NewDB(os.Getenv("ORACLE_DSN"), nil)
}
