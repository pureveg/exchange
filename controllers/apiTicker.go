package controllers

import (
	"bitbucket.org/pureveg/exchange/errCode"
)

type TickerController struct {
	baseController
}

// get ticker, pair specifies which ticker to get
func (this *TickerController) Get() {
	var out map[string]interface{}

	pair := this.Ctx.Input.Params[":objectId"]
	// TODO: return the data
	// possible output is PairNonExist | ticker data
	out["errCode"] = errCode.ErrNonExistPair
	this.Data["json"] = pair
	this.ServeJson()
}
