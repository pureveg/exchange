package controllers

import "encoding/json"
import "bitbucket.org/pureveg/exchange/errCode"

type NewsControllers struct {
	baseController
}

type news struct {
	Id      int64  `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Date    int64  `json:"date"`
	Author  string `json:"author"`
	Cid     int64  `json:"cid"`
}

// get the first 10 newest news
func (this *NewsControllers) Get() {
	// TODO: return the newest 10 news
	this.Data["json"] = []news{}
	this.ServeJson()
}

// post new news
func (this *NewsControllers) Post() {
	var (
		in struct {
			news
			Token string `json:"token"`
		}
		err error
		out map[string]interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err = json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out["errCode"] = errCode.ErrJsonUnmarshal
		return
	}

	// TODO: check if token is valid and store it in the database
	// possible errCode should be NoError | ErrSys | TokenInvalid
	out["json"] = errCode.NoError
}

// delete the specific news
func (this *NewsControllers) Delete() {
	var (
		in struct {
			Id    int64  `json:"id"`
			Cid   int64  `json:"cid"`
			Token string `json:"string"`
		}
		err error
		out map[string]interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err = json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out["errCode"] = errCode.ErrJsonUnmarshal
		return
	}

	// TODO: delete the news
	// possible errCode should be NoError | NewNonExist | TokenError
	out["errCode"] = errCode.NoError
}
