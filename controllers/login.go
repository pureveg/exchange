package controllers

import (
	"bitbucket.org/pureveg/exchange/errCode"
	"encoding/json"
)

type LoginController struct {
	baseController
}

type input struct {
	Type     string `json:"type"`
	Token    string `json:"access_token"`
	Name     string `json:"name"`
	Password string `json:"password"`
	GA       string `json:"ga"`
}

func (this *LoginController) Post() {
	var (
		err error
		in  input
		out map[string]interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err = json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out["errCode"] = errCode.ErrJsonUnmarshal
		return
	}

	switch in.Type {
	case "user":
		// TODO: check if user login success
		// possible errCode should be NoError / NonExistUser / IncorrectPassword
		out["errCode"] = errCode.NoError
		out["token"] = ""
		return

	case "qq", "google":
		// TODO: check if token matches
		// possible errCode should be NoError / Invalid token
		out["errCode"] = errCode.NoError
		out["token"] = ""
		return
	}

	out["errCode"] = errCode.ErrTypeInvalid
}
