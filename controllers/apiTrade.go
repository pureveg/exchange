package controllers

import (
	"bitbucket.org/pureveg/exchange/errCode"
	"encoding/json"
)

type TradeController struct {
	baseController
}

// get trade, pair specifies which trade to get
func (this *TradeController) Get() {
	var out interface{}
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	// pair
	pair := this.Ctx.Input.Param(":objectId")

	// TODO: return the trade data
	// possible output is PairNonExist | history data
	if pair == "" {
		out = errCode.ErrNonExistPair
		return
	}

	out = "1"
}

// place an order
func (this *TradeController) Post() {
	var (
		in struct {
			Pair    string  `json:"pair"`
			Rate    float64 `json:"rate"`
			Volumn  float64 `json:"vol"`
			Passkey string  `json:"passkey"`
			Token   string  `json:"token"`
		}
		out map[string]interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out["errCode"] = errCode.ErrJsonUnmarshal
		return
	}

	// TODO: place an order
	// possible errCode should be ....
}

// cancel an order
func (this *TradeController) Delete() {
	var (
		in struct {
			Pair      string `json:"pair"`
			Timestamp int64  `json:"timestamp"`
			Passkey   string `json:"passkey"`
			Token     string `json:"token"`
		}
		out map[string]interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out["errCode"] = errCode.ErrJsonUnmarshal
		return
	}

	// TODO: cancel an order
	// possible errCode should be ....
}
