package controllers

import (
	"bitbucket.org/pureveg/exchange/errCode"
	"encoding/json"
)

type HistoryController struct {
	baseController
}

// get history
// 1. pair specifies which history to get
// 2. since means only fetch the history data after since
func (this *HistoryController) Get() {
	var (
		in struct {
			Pair  string `json:"pair"`
			Since int64  `json:"since"`
		}
		out interface{}
	)
	defer func() {
		this.Data["json"] = out
		this.ServeJson()
	}()

	err := json.Unmarshal(this.Ctx.Input.RequestBody, &in)
	if err != nil {
		out = map[string]interface{}{
			"errCode": errCode.ErrJsonUnmarshal,
		}
		return
	}

	// TODO: return the history data
	// possible output is PairNonExist | history data
	out = "1"
}
